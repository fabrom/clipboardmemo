const fs = require('fs')
const chai = require('chai')
const assert = chai.assert
const DatabaseSingleton = require('../../assets/js/database/database')

const MAXITEMS = 80

describe('Database class tests', function () {

  let db1 = null

  before(function () {
    db1 = DatabaseSingleton.getInstance()
    db1.init()
    db1.remove()
  })

  it('database instance should be a singleton', function () {
    let db2 = DatabaseSingleton.getInstance()
    assert.strictEqual(db1, db2)
  })

  it('after init, file should exists', function () {
    db1.init()
    let fpath = db1.config.path
    assert.isTrue(fs.existsSync(fpath), fpath)
  })

  it('after remove, file should not exists', function () {
    let fpath = db1.config.path
    db1.remove()
    assert.isFalse(fs.existsSync(fpath), fpath)
  })

  it('after add entry, db is updated', function () {
    let ts = new Date()
    db1.init()
    db1.addRecord('foo1', 'text/plain')
    assert.isTrue(fs.existsSync(db1.config.path))
    assert.equal(db1.records[0].content, 'foo1')
    assert.equal(db1.records[0].type, 'text/plain')
    assert.isAtMost(ts, db1.records[0].timestamp)
  })

  it('after add entry, count is updated', function () {
    assert.equal(db1.count(), 1)
  })

  it('after clear db, count is updated', function () {
    db1.clear()
    assert.equal(db1.count(), 0)
    assert.equal(db1.size(), 0)
  })

  it('after add entry to full db, entries rotated', function () {
    for (let i = 0; i < MAXITEMS; i++) {
      db1.addRecord('foo' + i, 'text/plain')
    }
    assert.equal(db1.get(0).content, 'foo79')
    assert.equal(db1.get(79).content, 'foo0')
    assert.equal(db1.count(), MAXITEMS)
    db1.addRecord('fooZ', 'text/plain')
    assert.equal(db1.get(0).content, 'fooZ')
    assert.equal(db1.get(79).content, 'foo1')
    assert.equal(db1.count(), MAXITEMS)
  })

  it('return correct db full size', function () {
    assert.equal(db1.size(), 3110)
  })

  it('set correctly the maxitems attr', function () {
    db1.setRecordsMax(100)
    assert.equal(db1.maxitems, 100)
    db1.setRecordsMax(MAXITEMS)
    assert.equal(db1.maxitems, MAXITEMS)
  })

  it('getAll returns all records', function () {
    let records = db1.getAll()
    //console.log(records)
    assert.equal(records.length, 80)
  })

  it('get returns right record', function () {
    assert.equal(db1.get(0).content, 'fooZ')
  })

  it('full db rotate after adding new record', function () {
    assert.equal(db1.maxitems, MAXITEMS)
    assert.equal(db1.count(), MAXITEMS)
    assert.equal(db1.get(0).content, 'fooZ')
    db1.addRecord('fooY', 'text/plain')
    assert.equal(db1.count(), MAXITEMS)
    assert.equal(db1.get(0).content, 'fooY')
    assert.equal(db1.get(1).content, 'fooZ')
  })

  it('delete record with index', function () {
    db1.delRecord(1)
    assert.equal(db1.count(), 79)
    assert.equal(db1.get(0).content, 'fooY')
    assert.equal(db1.get(1).content, 'foo79')
    db1.delRecord(0)
    assert.equal(db1.count(), 78)
    assert.equal(db1.get(0).content, 'foo79')
  })

  it('delete not existing record is not possible', function () {
    assert.isFalse(db1.delRecord(500))
  })

  it('reinit instance should reload existing database', function () {
    db1.maxitems = 100
    db1.init()
    assert.equal(db1.maxitems, MAXITEMS)
    assert.equal(db1.count(), 78)
    assert.equal(db1.get(0).content, 'foo79')
  })

  it('after remove, db file does not exists anymore', function () {
    db1.remove()
    assert.isFalse(fs.existsSync(db1.config.path))
  })

  // FIXME : missing init function with options parameter tests

  after (function () {
  })
})
