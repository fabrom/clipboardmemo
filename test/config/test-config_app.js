const chai = require('chai')
const assert = chai.assert
const config = require('../../assets/js/config/app')

describe('Config module tests', function () {

  it('function get should return key value', function () {
    assert.equal(config.get('app.dock.items.labelMaxChars'), 24)
    assert.equal(config.get('app.shortcuts.pop'), 'CommandOrControl+Alt+V')
  })

  it('function get should return default if key doesn t exists', function () {
    assert.equal(config.get('app.dock.items.fooKey', 123), 123)
    assert.equal(config.get('fooKey', 'test'), 'test')
    assert.equal(config.get('fooKey'), undefined)
  })

})
